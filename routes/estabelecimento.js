import EstabelecimentoController from '../controllers/estabelecimento';

export default (app) => {
  const estabelecimentoController = new EstabelecimentoController(app.datasource.models.Estabelecimento);

  app.route('/estabelecimento')
    .get((req, res) => {
      estabelecimentoController.getAll()
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .post((req, res) => {
      estabelecimentoController.create(req.body)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    });

  app.route('/estabelecimento/:CODEST')
    .get((req, res) => {
      estabelecimentoController.getById(req.params)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .put((req, res) => {
      estabelecimentoController.update(req.body, req.params)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .delete((req, res) => {
      estabelecimentoController.delete(req.params)
        .then((response) => {
          res.sendStatus(response.statusCode);
        });
    });

    app.route('/recomendacoes')
    .get((req, res) => {
      estabelecimentoController.getListRecomendacoes()
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    });
};
