import ProdutoController from '../controllers/produto';

export default (app) => {
  const produtoController = new ProdutoController(app.datasource.models.Produto);

  app.route('/produto')
    .get((req, res) => {
      produtoController.getAll()
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .post((req, res) => {
      produtoController.create(req.body)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    });

  app.route('/produto/:CODPROD')
    .get((req, res) => {
      produtoController.getById(req.params)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .put((req, res) => {
      produtoController.update(req.body, req.params)
        .then((response) => {
          res.status(response.statusCode);
          res.json(response.data);
        });
    })
    .delete((req, res) => {
      produtoController.delete(req.params)
        .then((response) => {
          res.sendStatus(response.statusCode);
        });
    });
};
