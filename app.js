import bodyParser from 'body-parser';
import usersRouter from './routes/users';
import estabelecimentoRouter from './routes/estabelecimento';
import produtoRouter from './routes/produto';
import datasource from './config/datasource';
import config from './config/config';

const express = require('express')
const cors = require('cors');

const app = express();
app.config = config;
app.set('port', 3000);
app.datasource = datasource(app);

app.use(bodyParser.json());
app.use(cors());

usersRouter(app);
estabelecimentoRouter(app);
produtoRouter(app);

export default app;
