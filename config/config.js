export default {
  database: 'bdbeerpoint',
  username: 'root',
  password: '',
  params: {
    dialect: 'mysql',
    storage: `${process.env.NODE_ENV}_beerpoint.db`,
    define: {
      underscored: true,
    },
  },
};
