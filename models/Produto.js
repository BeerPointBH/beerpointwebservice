export default (sequelize, DataType) => {
  const Produto = sequelize.define('Produto', {
    CODPROD: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    NOMPROD: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    DESCPROD: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    VALPROD: {
      type: DataType.FLOAT,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    }
  });
  return Produto;
};
