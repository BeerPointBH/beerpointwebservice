export default (sequelize, DataType) => {
  const Estabelecimento = sequelize.define('Estabelecimento', {
    CODEST: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    NOMEST: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    CNPJEST: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    ENDEST: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    HRINIFUNC: {
      type: DataType.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    HRFIMFUNC: {
      type: DataType.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    }
  });
  return Estabelecimento;
};
