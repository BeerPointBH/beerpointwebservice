export default (sequelize, DataType) => {
  const Usuario = sequelize.define('Usuario', {
    CODUSU: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    NOMUSU: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    EMAIL: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    SENHA: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    DTNASCIMENTO: {
      type: DataType.DATE,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
  });
  return Usuario;
};
