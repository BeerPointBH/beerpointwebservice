import HttpStatus from 'http-status';

describe('Routes: Countries', () => {
  const Countries = app.datasource.models.Countries;

  const defaultCountry = {
    id: 1,
    abbreviation: 'BR',
    name: 'Brasil',
  };

  beforeEach(done => {
    Countries
    .destroy({ where: {} })
    .then(() => Countries.create(defaultCountry))
    .then(() => {
      done();
    });
  });

  describe('GET /countries', () => {
    it('should return a list of countries', done => {
      request
      .get('/countries')
      .end((err, res) => {
        expect(res.body[0].name).to.eql(defaultCountry.name);
        expect(res.body[0].abbreviation).to.eql(defaultCountry.abbreviation);
        expect(res.body[0].id).to.eql(defaultCountry.id);
        done(err);
      });
    });
  });

  describe('GET /countries/{id}', () => {
    it('should return a country by id', done => {
      request
      .get('/countries/1')
      .end((err, res) => {
        expect(res.body.name).to.eql(defaultCountry.name);
        expect(res.body.abbreviation).to.eql(defaultCountry.abbreviation);
        expect(res.body.id).to.eql(defaultCountry.id);
        done(err);
      });
    });
  });

  describe('POST /countries', () => {
    it('should post a country', done => {
      const country = {
        id: 2,
        abbreviation: 'MX',
        name: 'Mexico',
      };

      request
      .post('/countries')
      .send(country)
      .end((err, res) => {
        expect(res.body.name).to.eql(country.name);
        expect(res.body.id).to.eql(country.id);
        expect(res.body.abbreviation).to.eql(country.abbreviation);
        done(err);
      });
    });
  });

  describe('PUT /countries/{id}', () => {
    it('should update a country', done => {
      const country = {
        id: 1,
        abbreviation: 'BRA',
        name: 'Brazil',
      };

      request
      .put('/countries/1')
      .send(country)
      .end((err, res) => {
        expect(res.body).to.eql([1]);
        done(err);
      });
    });
  });

  describe('DELETE /countries/{id}', () => {
    it('should delete a country', done => {
      request
      .delete('/countries/1')
      .end((err, res) => {
        expect(res.statusCode).to.eql(HttpStatus.NO_CONTENT);
        done(err);
      });
    });
  });
});
