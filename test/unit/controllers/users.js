import UsersController from '../../../controllers/users';

describe('Controllers: Users', () => {
  describe('Get all users: getAll()', () => {
    it('Should return a list users', () => {
      const Users = {
        findAll: td.function(),
      };

      const expectedResponse = [{
        id: 1,
        name: 'Fellipe Abreu',
        created_at: '',
        update_at: '',
      }];

      td.when(Users.findAll({})).thenResolve(expectedResponse);

      const usersController = new UsersController(Users);
      return usersController.getAll()
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });

  describe('Get all users: getById()', () => {
    it('Should return a user', () => {
      const Users = {
        findOne: td.function(),
      };

      const expectedResponse = [{
        id: 1,
        name: 'Fellipe Abreu',
        created_at: '',
        update_at: '',
      }];

      td.when(Users.findOne({ where: { id: 1 } })).thenResolve(expectedResponse);

      const usersController = new UsersController(Users);
      return usersController.getById({ id: 1 })
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });

  describe('Create a user: create()', () => {
    it('Should create a user', () => {
      const Users = {
        create: td.function(),
      };

      const requestBody = {
        name: 'Fellipe Abreu',
      };

      const expectedResponse = [{
        id: 1,
        name: 'Fellipe Abreu',
        created_at: '',
        update_at: '',
      }];

      td.when(Users.create(requestBody)).thenResolve(expectedResponse);

      const usersController = new UsersController(Users);
      return usersController.create(requestBody)
        .then((response) => {
          expect(response.data).to.be.eql(expectedResponse);
          expect(response.statusCode).to.be.eql(201);
        });
    });
  });

  describe('Update a user: update()', () => {
    it('Should update a user', () => {
      const Users = {
        update: td.function(),
      };

      const requestBody = {
        id: 1,
        name: 'Fellipe S Abreu',
      };

      const expectedResponse = {
        id: 1,
        name: 'Fellipe S Abreu',
        created_at: '',
        update_at: '',
      };

      td.when(Users.update(requestBody, { where: { id: 1 } })).thenResolve(expectedResponse);

      const usersController = new UsersController(Users);
      return usersController.update(requestBody, { id: 1 })
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });

  describe('Delete a user: delete()', () => {
    it('Should delete a user', () => {
      const Users = {
        destroy: td.function(),
      };

      td.when(Users.destroy({ where: { id: 1 } })).thenResolve({});

      const usersController = new UsersController(Users);
      return usersController.delete({ id: 1 })
        .then(response => expect(response.statusCode).to.be.eql(204));
    });
  });
});
