import HttpStatus from 'http-status';

describe('Routes: Countries', () => {
  const Countries = app.datasource.models.Countries;

  const defaultCountry = {
    id: 1,
    abbreviation: 'BR',
    name: 'Brasil',
  };

  beforeEach(done => {
    Countries
    .destroy({ where: {} })
    .then(() => Countries.create(defaultCountry))
    .then(() => {
      done();
    });
  });

  describe('GET /countries', () => {
    it('should validate a list of countries', done => {
      request
      .get('/countries')
      .end((err, res) => {
        const countriesList = Joi.array().items(Joi.object().keys({
          id: Joi.number(),
          abbreviation: Joi.string(),
          name: Joi.string(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso(),
        }));

        joiAssert(res.body, countriesList);
        done(err);
      });
    });
  });

  describe('GET /countries/{id}', () => {
    it('should validate a single country schema', done => {
      request
      .get('/countries/1')
      .end((err, res) => {
        const countriesList = Joi.object().keys({
          id: Joi.number(),
          abbreviation: Joi.string(),
          name: Joi.string(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso(),
        });

        joiAssert(res.body, countriesList);
        done(err);
      });
    });
  });

  describe('POST /countries', () => {
    it('should validate a new country schema', done => {
      const country = {
        id: 2,
        abbreviation: 'MX',
        name: 'Mexico',
      };

      request
      .post('/countries')
      .send(country)
      .end((err, res) => {
        const createdCountry = Joi.object().keys({
          id: Joi.number(),
          abbreviation: Joi.string(),
          name: Joi.string(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso(),
        });

        joiAssert(res.body, createdCountry);
        done(err);
      });
    });
  });

  describe('PUT /countries/{id}', () => {
    it('should validate a updated country', done => {
      const country = {
        id: 1,
        abbreviation: 'BRA',
        name: 'Brazil',
      };

      request
      .put('/countries/1')
      .send(country)
      .end((err, res) => {
        const updatedCount = Joi.array().items(1);

        joiAssert(res.body, updatedCount);
        done(err);
      });
    });
  });

  describe('DELETE /countries/{id}', () => {
    it('should validate a deleted country', done => {
      request
      .delete('/countries/1')
      .end((err, res) => {
        expect(res.statusCode).to.eql(HttpStatus.NO_CONTENT);
        done(err);
      });
    });
  });
});
