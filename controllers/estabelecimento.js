import HttpStatus from 'http-status';

const defaultResponse = (data, statusCode = HttpStatus.OK) => ({
  data,
  statusCode,
});

const errorReponse = (message, statusCode = HttpStatus.BAD_REQUEST) => defaultResponse({
  error: message,
}, statusCode);

class EstabelecimentoController {
  constructor(estabelecimento) {
    this.estabelecimento = estabelecimento;
  }

  getAll() {
    return this.estabelecimento.findAll({})
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  getListRecomendacoes() {
    return this.estabelecimento.findAll({limit:5}) 
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  getById(params) {
    return this.estabelecimento.findOne({ where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  create(data) {
    return this.estabelecimento.create(data)
      .then(result => defaultResponse(result, HttpStatus.CREATED))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  update(data, params) {
    return this.estabelecimento.update(data, { where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  delete(params) {
    return this.estabelecimento.destroy({ where: params })
      .then(result => defaultResponse(result, HttpStatus.NO_CONTENT))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
}

export default EstabelecimentoController;
