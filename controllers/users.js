import HttpStatus from 'http-status';

const defaultResponse = (data, statusCode = HttpStatus.OK) => ({
  data,
  statusCode,
});

const errorReponse = (message, statusCode = HttpStatus.BAD_REQUEST) => defaultResponse({
  error: message,
}, statusCode);

class UsuarioController {
  constructor(Usuario) {
    this.Usuario = Usuario;
  }

  getAll() {
    return this.Usuario.findAll({})
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  getById(params) {
    return this.Usuario.findOne({ where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  create(data) {
    return this.Usuario.create(data)
      .then(result => defaultResponse(result, HttpStatus.CREATED))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  update(data, params) {
    return this.Usuario.update(data, { where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  delete(params) {
    return this.Usuario.destroy({ where: params })
      .then(result => defaultResponse(result, HttpStatus.NO_CONTENT))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
}

export default UsuarioController;
