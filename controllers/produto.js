import HttpStatus from 'http-status';

const defaultResponse = (data, statusCode = HttpStatus.OK) => ({
  data,
  statusCode,
});

const errorReponse = (message, statusCode = HttpStatus.BAD_REQUEST) => defaultResponse({
  error: message,
}, statusCode);

class ProdutoController {
  constructor(Produto) {
    this.Produto = Produto;
  }

  getAll() {
    return this.Produto.findAll({})
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  getById(params) {
    return this.Produto.findOne({ where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message));
  }

  create(data) {
    return this.Produto.create(data)
      .then(result => defaultResponse(result, HttpStatus.CREATED))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  update(data, params) {
    return this.Produto.update(data, { where: params })
      .then(result => defaultResponse(result))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  delete(params) {
    return this.Produto.destroy({ where: params })
      .then(result => defaultResponse(result, HttpStatus.NO_CONTENT))
      .catch(error => errorReponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
}

export default ProdutoController;
